# GetYourGuide

Get Your Guide take home test app project

## Problem statement

> “As a potential traveler, I want to read a list of reviews for one of our most popular Berlin tours.”

> “As a potential traveler, I want to just scroll down to read all the reviews for one of our  most popular Berlin tours.”

 
> The following web service delivers n (count) reviews which include the review author, title, message, date, rating, language code:

 
> https://www.getyourguide.com/berlin-l17/tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776/reviews.json?count=5&page=0&rating=0&sortBy=date_of_review&direction=DESC

> URL params

> Required

> count=[integer]

> page=[integer]

 
> Optional

> rating=[integer 0..5]

> sortBy=[string date_of_review/rating]

> direction=[string asc/desc]



## Installation

git clone https://bitbucket.org/dhomes/getyourguide.git

Then:

```bash
pod install
```

## Platform
iOS 12.2 / Xcode 10.2.1

## Libraries Used
  - Just : Swift HTTP for Humans | [Website](https://github.com/dduan/Just) | MIT
  - Hex:  Hex support for UIColor, all in Swift | [Website](https://github.com/3lvis/Hex) | MIT
  - SnapKit: A Swift Autolayout DSL for iOS & OS X | [Website](https://github.com/SnapKit/SnapKit) | MIT
  - ISMessages: simple extension for presenting system-wide notifications from top/bottom of device screen | [Website](https://github.com/ilyainyushin/ISMessages) | MIT
  - MXParallaxHeader: Simple parallax header for UIScrollView | [Website](https://github.com/maxep/MXParallaxHeader) | MIT
  - Ji: an XML/HTML parser for Swift | [Website](https://github.com/honghaoz/Ji) | MIT
  - HydraAsync: Lightweight full-featured Promises, Async & Await Library in Swift | [Website](https://github.com/malcommac/Hydra) | MIT

## Author
David Homes / dhomes@gmail.com

## License
[MIT](https://choosealicense.com/licenses/mit/)
