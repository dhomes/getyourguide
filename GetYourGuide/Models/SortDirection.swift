//
//  SortDirection.swift
//  GetYourGuide
//
//  Created by dhomes on 7/12/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation

/// Sort Direction for ReviewQuery
///
enum SortDirection : String, CustomStringConvertible {
    case ascending = "asc"
    case descending = "desc"
    var description: String {
        switch self {
        case .ascending:
            return "ascending"
        case .descending:
            return "descending"
        }
    }
}
