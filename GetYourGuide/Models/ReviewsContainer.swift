//
//  ReviewContainer.swift
//  GetYourGuide
//
//  Created by dhomes on 7/12/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation


/// The container of the Reviews from network
struct ReviewsContainer : Codable {
    let status : Bool
    let totalComments : Int
    let reviews : [Review]
    
    enum CodingKeys : String, CodingKey {
        case status
        case totalComments = "total_reviews_comments"
        case reviews = "data"
    }
    
}


