//
//  LocationStore.swift
//  GetYourGuide
//
//  Created by dhomes on 7/12/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation
import Ji

typealias SiteFetchCompletion = (Result<Location,NetworkError>) -> ()

/// Parses a location's url and extracts image & metadata from page's xml, stores to user defaults
/// - Discussion : Currently only handles BerlinAirport, uses Ji library for parsing
final class LocationStore  {
    
    private let workQueue = OperationQueue()
    
    func info(completion : @escaping SiteFetchCompletion) {
        
        
        // get from cache
        if let cached : Location = Defaults.getCustomObject(for: .location) {
            completion(.success(cached))
            return
        }
        
        // if not get from remote
        Network().getData(for: BerlinAirport.site, returnQueue: workQueue) { (result) in
            
            let mainQueue = OperationQueue.main
            
            switch result {
            case .failure(let e):
                mainQueue.addOperation { completion(.failure(e)) }
                
            case .success(let data):
                guard let string = String(data: data, encoding: .utf8),
                    let ji = Ji.init(htmlString: string)  else {
                    mainQueue.addOperation { completion(.failure(.noData)) }
                    return
                }
                let headNode = ji.rootNode?.firstChildWithName("head")
                
                var title : String? = nil
                var description : String? = nil
                var imageUrl : String? = nil
                
                headNode?.childrenWithName("meta").filter { $0.attributes["property"] != nil}.filter { $0.attributes["content"] != nil}.forEach {

                    if $0.attributes["property"] == "og:title", let value = $0.attributes["content"] {
                        title = value
                    }
                    if $0.attributes["property"] == "og:description", let value = $0.attributes["content"] {
                        description = value
                    }
                    if $0.attributes["property"] == "og:image", let value = $0.attributes["content"] {
                        imageUrl = value
                    }
                    
                }
                
                guard let t = title, let d = description, let i = imageUrl else {
                    mainQueue.addOperation { completion(.failure(.noData)) }
                    return
                }
                
                let location = Location(title: t, description: d, imageAt: i)
                location.getImage(completion: { _ in
                    // in main
                    Defaults.setCustomObject(location, forKey: .location)
                    completion(.success(location))
                })
                
            }
        }
    }
}
