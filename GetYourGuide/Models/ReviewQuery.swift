//
//  Request.swift
//  GetYourGuide
//
//  Created by dhomes on 7/12/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation

/// The murable ReviewQuery we'll use to handle page, count, and criterias
/// - Discussion: objc : NSCoding so we can store to user defaults
final class ReviewQuery : NSObject, NSCoding, QueryRepresentable {
    
    static let defaultFetchCount = 10
    
    var count : Int
    var page : Int
    var rating : Rating? = nil
    var sortCriteria : SortCriteria? = nil
    var sortDirection : SortDirection? = nil
    
    func increasePage() {
        page += 1
    }
    
    func decreasePage() {
        page -= 1
    }
    
    var query: [String : Any] {
        var q : [String : Any] = [
            "count" : count,
            "page" : page,
         ]
        if let rating = rating {
            q["rating"] = rating.rawValue
        }
        if let criteria = sortCriteria {
            q["sortBy"] = criteria.rawValue
        }
        if let direction = sortDirection {
            q["direction"] = direction.rawValue
        }
        return q
    }

    init(count: Int,
         initialPage page : Int = 0,
         rating : Rating? = nil,
         sortCriteria criteria : SortCriteria? = nil,
         sortDirection direction : SortDirection? = nil) {
        
        self.count = count
        self.page = page
        self.rating = rating
        self.sortCriteria = criteria
        self.sortDirection = direction
    }
    
    static var defaultQuery : ReviewQuery {
        return ReviewQuery(count: defaultFetchCount, initialPage: 0)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(count, forKey: "count")
        // no need to store the page
        if let rating = self.rating {
            aCoder.encode(rating.rawValue, forKey: "rating")
        }
        if let sort = self.sortCriteria {
            aCoder.encode(sort.rawValue, forKey: "sortCriteria")
        }
        if let direction = self.sortDirection {
            aCoder.encode(direction.rawValue, forKey: "sortDirection")
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        let count = aDecoder.decodeInt32(forKey: "count")
        
        self.count = Int(count)
        self.page = 0
        let ratingValue = Int(aDecoder.decodeInt32(forKey: "rating"))
        if let rating = Rating(rawValue: ratingValue) {
            self.rating = rating
        }
        
        if let sortCriteriaString = aDecoder.decodeObject(forKey: "sortCriteria") as? String,
            let sortCriteria = SortCriteria(rawValue: sortCriteriaString) {
            self.sortCriteria = sortCriteria
        }
        
        if let sortDirectionString = aDecoder.decodeObject(forKey: "sortDirection") as? String,
            let sortDirection = SortDirection(rawValue: sortDirectionString) {
            self.sortDirection = sortDirection
        }
        super.init()
    }
    
    override var description: String {
        
        let r = rating != nil ? "\(rating!.rawValue)" : "not set"
        let sc = sortCriteria != nil ? "\(sortCriteria!.rawValue)" : "not set"
        let sd = sortDirection != nil ? "\(sortDirection!.rawValue)" : "not set"
        
        let d =
        """
        count: \(count)
        page: \(page)
        rating: \(r)
        sort criteria: \(sc)
        sort direction: \(sd)
        """
        return d
    }
}


