//
//  Defaults.swift
//  GetYourGuide
//
//  Created by dhomes on 7/13/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import UIKit

/// Keys for storing to UserDefaults
enum DefaultsKey : String  {
    case location
    case reviewQuery
}

/// Helper struct for storing and retriving foundation & custom objects
struct Defaults {
    
    private static let defaults = UserDefaults.standard
    
    static func removeObject(for key : DefaultsKey) {
        defaults.removeObject(forKey: key.rawValue)
    }
    
    static func setValue(_ value : Any, forKey key: DefaultsKey) {
        defaults.set(value, forKey: key.rawValue)
    }
    
    static func getValue<T>(for key : DefaultsKey) -> T? {
        return defaults.object(forKey: key.rawValue) as? T
    }
    
    @discardableResult
    static func setCustomObject<T : NSObject & NSCoding>(_ value : T, forKey key: DefaultsKey) -> Bool {
        guard let data = try? NSKeyedArchiver.archivedData(withRootObject: value, requiringSecureCoding: false) else {
            return false
        }
        defaults.set(data, forKey: key.rawValue )
        return true
     }
    
    static func getCustomObject<T : NSObject & NSCoding>(for key : DefaultsKey) -> T? {
        guard let data = defaults.object(forKey: key.rawValue) as? Data else {
            return nil
            
        }
        let object = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data)
        return object as? T
    }
    
    static func getImage(named name: String) -> UIImage? {
        var image: UIImage?
        if let data = defaults.data(forKey: name) {
            image = try? NSKeyedUnarchiver.unarchivedObject(ofClass: UIImage.self, from: data)
        }
        return image
    }
    
    static func saveImage(image: UIImage?, forName key: String) {
        var data: Data?
        if let image = image {
            data = try? NSKeyedArchiver.archivedData(withRootObject: image, requiringSecureCoding: false)
        }
        defaults.set(data, forKey: key)
    }
}
