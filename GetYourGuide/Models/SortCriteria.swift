//
//  SortCriteria.swift
//  GetYourGuide
//
//  Created by dhomes on 7/12/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation

/// Sort Criteria for ReviewQuery
enum SortCriteria : String, CustomStringConvertible {
    case byDate = "date_of_review"
    case rating = "rating"
    var description: String {
        switch self {
        case .byDate:
            return "by date"
        case .rating:
            return "by rating"
        }
    }
}

