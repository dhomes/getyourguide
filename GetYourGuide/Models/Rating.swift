//
//  Rating.swift
//  GetYourGuide
//
//  Created by dhomes on 7/12/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation

/// Enumerator for the review stars, used by ReviewQuery
enum Rating : Int, CustomStringConvertible {
    case noStars = 0
    case one = 1
    case two = 2
    case three = 3
    case four = 4
    case five = 5
    var description: String {
        
        switch self {
        case .noStars:
            return "all ratings"
        case .one:
            return "one star"
        case .two:
            return "two stars"
        case .three:
            return "three stars"
        case .four:
            return "four stars"
        case .five:
            return "five stars"
        }
    }
}
