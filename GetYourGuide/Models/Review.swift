//
//  Review.swift
//  GetYourGuide
//
//  Created by dhomes on 7/12/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation

/* NOTES:
  - date_unformatted always nil even on a 100 sample fetch
 ignoring from struct
 
  - reviewProfilePhoto appears to be always be nil
 
  - reviewer's name is "isAnonymous" & firstInitial is empty string on isAnonymous = true
 
  - payload shows mixture of camel case & serpent case notation (review_id, date_unformatted vs reviewerCountry, reviewerProfilePhoto, etc)
 */

/// A Review object from network
struct Review : Codable {

    /// review_id key
    let id : Int
    
    /// review_id key
    let rating : String
    
    /// rating as a double value
    var ratingDouble : Double {
        return Double(rating) ?? 0.0
    }

    /// title key
    let title : String?
    
    /// message key, actual content of the review
    let message : String
    
    /// author key
    let author : String
    
    /// foreignLanguage key
    let foreignLanguage : Bool
    
    /// date key as String
    let dateString : String
    var date : Date? {
        return Date.fromNamedMonthDayYear(dateString)
    }
    
    /// language key
    let languageCode : String
    
    /// language_type key
    let travelerType : String?
    
    /// review's name
    let reviewerName : String

    let reviewerCountry : String
    
    /// url string to reviewer's profile photo, appears nil in 100 count sample fetch
    let reviewerProfilePhoto : String?
    
    /// isAnonymous key
    let isAnonymous : Bool
    
    /// first initial
    let firstInitial : String
    
    enum CodingKeys : String, CodingKey {
        case id = "review_id"
        case rating
        case title
        case message
        case author
        case foreignLanguage
        case dateString = "date"
        case languageCode
        case travelerType = "traveler_type"
        case reviewerName
        case reviewerCountry
        case reviewerProfilePhoto
        case isAnonymous
        case firstInitial
    }
}


