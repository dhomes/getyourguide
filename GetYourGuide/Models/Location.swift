//
//  Site.swift
//  GetYourGuide
//
//  Created by dhomes on 7/12/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import UIKit
import Just

/// Location object with title / description and imageUrl originally obtained from a location's url
/// - Discussion: objc : NSCoding for storing to defaults
@objc class Location : NSObject, NSCoding {
    
    let title : String
    let locationDescription : String
    let imageUrl : String
    
    private var privateImage : UIImage? = nil {
        didSet {
            if let image = privateImage {
                Defaults.saveImage(image: image, forName: title)
            }
        }
    }
    
    var image : UIImage? {
        if let image = privateImage { return image }
        let image = Defaults.getImage(named: title)
        privateImage = image
        return image
    }
    
    var widthToHeightRatio : CGFloat {
        guard let i = image, i.size.height > 0 else {
            return 0
        }
        return i.size.width / i.size.height
    }


    func getImage(completion: @escaping (UIImage?) -> ()) {

        if let i = image {
            completion(i)
            return
        }
        
        let main = DispatchQueue.main
        
        Just.get(imageUrl, asyncCompletionHandler: { [weak self](result) in
            guard let data = result.content, let image = UIImage(data: data) else {
                main.async {
                    completion(nil)
                }
                return
            }
            self?.privateImage = image
            main.async {
                completion(image)
            }
        })
        
    }

    init(title: String, description: String, imageAt imageLocation : String) {
        self.title = title
        self.locationDescription = description
        self.imageUrl = imageLocation
    }
    
    required init?(coder decoder: NSCoder) {

        guard let title = decoder.decodeObject(forKey: "title") as? String else {
            return nil
        }
        guard let locationDescription = decoder.decodeObject(forKey: "locationDescription") as? String else {
            return nil
        }
        guard let urlString = decoder.decodeObject(forKey: "imageUrl") as? String else {
            return nil
        }
        self.title = title
        self.locationDescription = locationDescription
        self.imageUrl = urlString
        
    }

    func encode(with coder: NSCoder) {
        coder.encode(self.title, forKey: "title")
        coder.encode(self.locationDescription, forKey: "locationDescription")
        coder.encode(self.imageUrl, forKey: "imageUrl")
    }
}
