//
//  GYGError.swift
//  GetYourGuide
//
//  Created by dhomes on 7/14/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation


/// Get Your Guide network error
/// - Discussion: only ran onto 1 when running out of reviews, true / false for status not quite clear if only for this case or possible others
struct GYGError : Error, Codable {
    let status : Bool
    let totalComments : Int
    let message : String
    enum CodingKeys : String, CodingKey {
        case status
        case totalComments = "total_reviews_comments"
        case message
    }
}
