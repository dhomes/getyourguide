//
//  ReviewStore.swift
//  GetYourGuide
//
//  Created by dhomes on 7/13/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation
import Hydra

/// Handles fetching to network and converting response to Reviews, maintains state of whether more reviews are available
final class ReviewStore {

    private var more = true
    var moreAvailable : Bool {
        return more
    }
    func getReviews(forQuery query : ReviewQuery) -> Promise<[Review]> {
        
        return Promise<[Review]>.init({ (resolve, reject, status) in
            Network().getData(for: BerlinAirport.reviews(query), completion: { result in
                switch result {
                case .failure(let e):
                    reject(e)
                case .success(let data):
                    let decoder = JSONDecoder()
                    let container = try? decoder.decode(ReviewsContainer.self, from: data)
                    guard let reviews = container?.reviews else {
                        if let gygError = try? decoder.decode(GYGError.self, from: data) {
                            self.more = gygError.status
                            reject(NetworkError.other(gygError))
                            return
                        }
                        reject(NetworkError.noData)
                        return
                    }
                    self.more = reviews.count == query.count
                    resolve(reviews)
                }
            })
        })
        
    }
}
