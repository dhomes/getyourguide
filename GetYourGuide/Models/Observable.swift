//
//  Observable.swift
//  GetYourGuide
//
//  Created by dhomes on 7/13/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation

/// Observable property pattern
class Observable<T> {
    
    var value: T {
        didSet {
            DispatchQueue.main.async {
                self.valueChanged?(self.value)
            }
        }
    }
    
    init(_ value : T) {
        self.value = value
    }
    
    var valueChanged: ((T) -> Void)?
}
