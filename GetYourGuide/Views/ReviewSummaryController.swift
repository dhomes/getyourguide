//
//  ReviewSummaryController.swift
//  GetYourGuide
//
//  Created by dhomes on 7/13/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import UIKit
import MXParallaxHeader
import SnapKit

class ReviewSummaryController: UITableViewController {

    private static let headerHeight : CGFloat = 36
    
    var headerModel = HeaderViewModel()
    var reviewsModel = ReviewListViewModel()
    let refresh = UIRefreshControl()
    let leftActivity = UIActivityIndicatorView(style: .white)
    var leftItem : UIBarButtonItem!
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // UI & INITIAL STATE
        let tintingColor = Styles.colors.barTintColor
        activity.alpha = 0
        activity.color = tintingColor
        tableView.separatorStyle = .none
        
        self.clearsSelectionOnViewWillAppear = true
        setTitleView()
        
        // register the cell
        tableView.register(ReviewCell.nib, forCellReuseIdentifier: ReviewCell.identifier)
        tableView.separatorColor = tintingColor
        tableView.backgroundColor = Styles.colors.ultraLightGray
        
        // REFRESH
        // Pre-iOS 10 to support parallax header + refresh
        refresh.addTarget(reviewsModel, action: #selector(reviewsModel.refresh(_:)), for: .valueChanged)
        refresh.tintColor = .red
        tableView.backgroundView = refresh
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: Styles.images.gear, style: .done, target: self, action: #selector(self.settings(_:)))
    
        // Left Item
        leftItem = UIBarButtonItem(customView: leftActivity)
        self.navigationItem.leftBarButtonItem = leftItem
        
        // Use default built in controller
        set(headerModel: nil, reviewsModel: nil)
    }

    func set(headerModel hm: HeaderViewModel?, reviewsModel rm : ReviewListViewModel?) {
        
        if let hm = hm {
            self.headerModel = hm
        }
        if let rm = rm {
            self.reviewsModel = rm
        }
        
        headerModel.headerView.valueChanged = { header in
            UIView.animate(withDuration: 0.25) { [weak self] in
                guard let parallax = self?.tableView.parallaxHeader else {
                    return
                }
                parallax.view = header
                parallax.height = HeaderView.height
                parallax.mode = .bottom
                parallax.minimumHeight = HeaderView.textHeight
            }
        }
        headerModel.getLocation()
        
        reviewsModel.reviews.valueChanged = { [weak self] reviews in
            guard let s = self else {
                return
            }
            s.tableView.reloadData()
        }
        reviewsModel.refreshed.valueChanged = { [weak self] direction in
            if let s = self  {
                s.activity.stopAnimating()
                s.activity.alpha = 0
                if s.reviewsModel.count > 0  {
                    s.tableView.separatorStyle = .singleLine
                    if direction == .top  {
                        s.refresh.endRefreshing()
                        s.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                    }
                }
            }
            
        }
        leftActivity.startAnimating()
        reviewsModel.fetch(direction: .bottom) { [weak self] in
            self?.leftActivity.stopAnimating()
            self?.navigationItem.leftBarButtonItem = nil
        }
        
    }
    
    @objc func settings(_ sender : Any?) {
        self.performSegue(withIdentifier: "present", sender: nil)
    }

}


//MARK: - DELEGATE METHODS
extension ReviewSummaryController {
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            reviewsModel.fetchIfNeeded(for: indexPath.row, indicator : activity)
        }
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400.0
    }
    
}

//MARK: - DATASOURCE METHODS
extension ReviewSummaryController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return reviewsModel.numberOfSections()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviewsModel.rowsInSection(section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "header")
            cell.textLabel?.textColor = Styles.colors.barTintColor
            cell.textLabel?.font = UIFont.systemFont(ofSize: 32, weight: .medium)
            cell.textLabel?.text = "All Reviews"
            cell.detailTextLabel?.text = reviewsModel.queryDescription()
            return cell
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReviewCell.identifier) as? ReviewCell else {
            return UITableViewCell()
        }
        let review = reviewsModel.reviewAt(indexPath: indexPath)
        cell.review = review
        return cell
    }
    
}

