//
//  ReviewCell.swift
//  GetYourGuide
//
//  Created by dhomes on 7/13/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import UIKit
import SnapKit

class ReviewCell: UITableViewCell {

    static let identifier = "ReviewCell"
    static var nib : UINib? {
        return UINib(nibName: self.identifier, bundle: nil)
    }
    @IBOutlet weak var bar: AARatingBar!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var reviewer: UILabel!
    
    var review : ReviewViewModel! {
        didSet {
            title.text = review.title
            bar.value = CGFloat(review.rating)
            message.text = review.message
            reviewer.text = review.reviewerText
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = .clear
        backgroundColor = .clear
        backgroundView?.backgroundColor = .clear
        bar.isEnabled = false
        bar.backgroundColor = .clear
        bar.color = Styles.colors.barTintColor
        bar.value = 0
        bar.maxValue = 5
        bar.isAbsValue = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.title.text = nil
        self.message.text = nil
        self.reviewer.text = nil
        self.bar.value = 0
    }
    
}
