//
//  HeaderView.swift
//  GetYourGuide
//
//  Created by dhomes on 7/13/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import UIKit
import SnapKit

class HeaderView: UIView {
    private static var privateTextHeight : CGFloat = 0
    private static var privateHeight : CGFloat = 0
    static var height : CGFloat {
        return privateHeight
    }
    static var textHeight : CGFloat {
        return privateTextHeight
    }
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    private weak var location : Location? = nil
    private var titleFont = UIFont.systemFont(ofSize: 22, weight: .medium)
    private var descriptionFont = UIFont.systemFont(ofSize: 14, weight: .thin)
    private var placeholderHeight : CGFloat = 0
    private var descriptionHeight : CGFloat = 0
    
    init(withLocation location : Location) {
        
        var frame = CGRect.zero
        if let _ = location.image {
            let b = UIScreen.main.bounds
            
            let titlePlaceholder = UILabel(frame: .zero)
            titlePlaceholder.font = titleFont
            titlePlaceholder.text = location.title
            titlePlaceholder.numberOfLines = 0
            
            let descriptionPlaceholder = UILabel(frame: .zero)
            descriptionPlaceholder.font = descriptionFont
            descriptionPlaceholder.text = location.locationDescription
            descriptionPlaceholder.numberOfLines = 0
            
            let width = b.size.width - 20
            
            placeholderHeight = titlePlaceholder.heightInWidth(width)
            descriptionHeight = descriptionPlaceholder.heightInWidth(width)
            
            let textHeight = placeholderHeight + descriptionHeight + 20
            
            HeaderView.privateHeight = (b.width / location.widthToHeightRatio) + textHeight
            
            frame = CGRect(x: 0, y: 0, width: b.width, height: HeaderView.height)
        }
        self.location = location
        super.init(frame: frame)
        commonInit()
    }

    override init(frame : CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder : NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("HeaderView", owner: self, options: nil)
        title.text = nil
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        addSubview(contentView)
        if let l = location {
            image.image = l.image
            
            title.font = titleFont
            title.text = l.title
            title.backgroundColor = .clear
            
            descriptionLabel.text = location?.locationDescription
            descriptionLabel.backgroundColor = .clear
            descriptionLabel.font = descriptionFont
            
            let v = contentView!
            let space : CGFloat = 10
            descriptionLabel.snp.makeConstraints { (m) in
                m.left.equalTo(v.snp.left).offset(space)
                m.right.equalTo(v.snp.right).offset(-space)
                m.bottom.equalTo(v.snp.bottom).offset(-space)
                m.top.equalTo(title.snp.bottom).offset(space)
            }
            
            title.snp.makeConstraints { (m) in
                m.left.equalTo(v.snp.left).offset(space)
                m.right.equalTo(v.snp.right).offset(-space)
                m.top.equalTo(image.snp.bottom).offset(space)
            }
            
            image.snp.makeConstraints { (m) in
                m.left.equalTo(v.snp.left)
                m.right.equalTo(v.snp.right)
                m.top.equalTo(v.snp.top)
            }


            
            UIView.evaluateConstraintsForView(view: self)
            let height = title.frame.height + descriptionLabel.frame.height + 30.0
            HeaderView.privateTextHeight = height
            

        }
        
    }

}
