//
//  SearchSettingsController.swift
//  GetYourGuide
//
//  Created by dhomes on 7/13/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import UIKit

extension UIApplication {
    static let QUERY_SETTINGS_CHANGED = Notification.Name.init(rawValue: "QUERY_SETTINGS_CHANGED")
}

/// Modifies default fetch query. Kept it in simple MVC for speed and because of its basic funcionality
class FetchSettingsController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var fetchSegment: UISegmentedControl!
    @IBOutlet weak var starsSegment: UISegmentedControl!
    @IBOutlet weak var sortSegment: UISegmentedControl!
    @IBOutlet weak var sortOrder: UISegmentedControl!
    
    var query : ReviewQuery {
        return Defaults.getCustomObject(for: .reviewQuery) ?? ReviewQuery.defaultQuery
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitleView()
        titleLabel.text = "Fetch\nSettings"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancel(_:)))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(self.save(_:)))
        view.backgroundColor = Styles.colors.barTintColor
        
        fetchSegment.selectedSegmentIndex = query.countIndex()
        starsSegment.selectedSegmentIndex = query.starIndex()
        sortSegment.selectedSegmentIndex = query.sortIndex()
        sortOrder.selectedSegmentIndex = query.directionIndex()
    }
    
    @objc func cancel(_ sender : Any?) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func save(_ sender : Any?) {
        
        let count = ReviewQuery.countForIndex(fetchSegment.selectedSegmentIndex)
        let rating = ReviewQuery.ratingForIndex(starsSegment.selectedSegmentIndex)
        let sortCriteria = ReviewQuery.sortCriteria(sortSegment.selectedSegmentIndex)
        let sortDirection = ReviewQuery.sortDirection(sortOrder.selectedSegmentIndex)
        let newQuery = ReviewQuery(count: count, initialPage: 0, rating: rating, sortCriteria: sortCriteria, sortDirection: sortDirection)
        
        if (Defaults.setCustomObject(newQuery, forKey: .reviewQuery)) {
           NotificationCenter.default.post(name: UIApplication.QUERY_SETTINGS_CHANGED, object: nil)
        }
        self.dismiss(animated: true, completion: nil)
    }

}
