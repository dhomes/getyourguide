//
//  UIView+Extensions.swift
//  GetYourGuide
//
//  Created by dhomes on 7/13/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import UIKit

extension UIView {
    static func evaluateConstraintsForView(view : UIView ) {
        for v in view.subviews {
            self.evaluateConstraintsForView(view: v)
        }
        
        view.setNeedsLayout()
        view.layoutIfNeeded()
    }

}

