//
//  UILabel+Extensions.swift
//  GetYourGuide
//
//  Created by dhomes on 7/13/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import UIKit

extension UILabel {
    
    func heightForText () -> CGFloat {
        guard let font = self.font else {
            return 0
        }
        let attributedText = NSAttributedString(string: self.text!, attributes: [NSAttributedString.Key.font:font])
        
        let rect = attributedText.boundingRect(with: CGSize(width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(rect.size.height)
    }
    
    func heightInWidth (_ width : CGFloat) -> CGFloat {
        guard let font = self.font else {
            return 0
        }
        let attributedText = NSAttributedString(string: self.text!, attributes: [NSAttributedString.Key.font:font])
        
        let rect = attributedText.boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(rect.size.height)
    }
}
