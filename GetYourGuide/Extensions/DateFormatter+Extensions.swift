//
//  DateFormatter+Extensions.swift
//  GetYourGuide
//
//  Created by dhomes on 7/12/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation

extension Date {
    
    /// Returns a date, if possible, from a "MonthName dd, YYYY" String (i.e.: March 11, 2013)
    ///
    /// - Parameter dateString: the string to parse
    /// - Returns: the date experessed by the string
    static func fromNamedMonthDayYear(_ dateString : String) -> Date? {
        let formatter = DateFormatter()
        let calendar = Calendar.init(identifier:  .gregorian)
        formatter.dateFormat = "MMMM dd, yyyy"
        formatter.locale = Locale.current
        formatter.calendar = calendar
        guard let date = formatter.date(from: dateString) else {
            return nil
        }
        
        let beginning = calendar.startOfDay(for: date)
        return beginning
    }
}
