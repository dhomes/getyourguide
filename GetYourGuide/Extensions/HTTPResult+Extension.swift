//
//  HTTPResult+Extension.swift
//  GetYourGuide
//
//  Created by dhomes on 7/12/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation
import Just

public extension HTTPResult {
    
    func httpCode() -> Int {
        var code : Int = 0
        if let response = self.response as? HTTPURLResponse {
            code = response.statusCode
        }
        return code
    }
    
    
    func hasValidHTTPCode() -> Bool {
        return 200...299 ~= self.httpCode()
    }
    
    func log(_ title : String? = nil) {
        
        #if !DEBUG
            return
        #endif
        
        if let t = title {
            print("********************************************")
            print(t)
            print("********************************************")
        }
        // REQUEST
        let reqEndpoint = self.request?.url?.description ?? "NIL REQUEST ENDPOINT"
        let reqMethod = self.request?.httpMethod ?? "NIL METHOD"
        let reqHeaders = self.request?.allHTTPHeaderFields
        
        var reqBody : [String : Any]? = nil
        var reqString : String? = nil
        
        if let data = self.request?.httpBody {
            reqBody = try? JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
            reqString = String(data: data, encoding: .utf8)
        }
        let resCode = self.httpCode()
        var resBody : [String : Any]? = nil
        if let data = self.content {
            resBody = try? JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
        }
        
        var responseString : String? = nil
        if let content = self.content {
            responseString = String(data: content, encoding: .utf8)
        }
        let resHeaders = self.headers
        
        let emptyJson = [String : Any]()
        
        let dateTime = Date().description(with: Locale.current)
        let log =
        """
        REQUEST:
        ----------------------------------
        Endpoint: \(reqEndpoint)
        
        Method: \(reqMethod)
        
        Headers: \(reqHeaders ?? [String:String]())
        
        Body:
        \(reqBody ?? emptyJson)
        
        Request String:
        \(reqString ?? "")
        
        RESPONSE:
        ----------------------------------
        HTTP CODE: \(resCode)
        
        HEADERS:
        \(resHeaders)
        
        BODY:
        \(resBody ?? emptyJson)
        
        RESPONSE STRING:
        \(responseString ?? "EMPTY")
        ----------------------------------
        
        LOG DATE:
        \(dateTime)
        """
        
        print(log)
        
    }
    
    
}


