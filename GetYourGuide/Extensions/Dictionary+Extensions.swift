//
//  Dictionary+Extensions.swift
//  GetYourGuide
//
//  Created by dhomes on 7/12/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation

extension Dictionary where Key == String, Value == Any {
    func data() -> Data? {
        do {
            let d = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions())
            return d
        } catch {
            return nil
        }
    }
    
    func urlQuery() -> String {
        if self.count == 0 {
            return ""
        }
        var q = "?"
        for (_, v) in self.enumerated() {
            q += "\(v.key)=\(v.value)&"
        }
        return String(q.dropLast())
    }
}
