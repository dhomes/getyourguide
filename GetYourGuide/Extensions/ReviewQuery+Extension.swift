//
//  ReviewQuery+Extension.swift
//  GetYourGuide
//
//  Created by dhomes on 7/14/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation

// Bit of a hack for ReviewQuery to SelectedSegmentIndexes, can be done via a map or struct inside ReviewQuery itself, keeping it for time
extension ReviewQuery {
    
    func countIndex() -> Int {
        switch count {
        case 5:
            return 0
        case 10:
            return 1
        case 15:
            return 2
        case 20:
            return 3
        default:
            return 1
        }
    }
    
    func starIndex() -> Int {
        guard let rating = self.rating else {
            return 0
        }
        return rating.rawValue
    }
    
    func sortIndex() -> Int {
        guard let sortBy = self.sortCriteria else {
            return 0
        }
        switch sortBy {
        case .byDate:
            return 1
        case .rating:
            return 2
        }
    }
    
    func directionIndex() -> Int {
        guard let order = self.sortDirection else {
            return 0
        }
        switch order {
        case .ascending:
            return 1
        case .descending:
            return 2
        }
    }
}

extension ReviewQuery {
    
    static func countForIndex(_ i : Int) -> Int {
        switch i {
        case 0:
            return 5
        case 1:
            return 10
        case 2:
            return 15
        case 3:
            return 20
        default:
            return ReviewQuery.defaultFetchCount
        }
    }
    
    static func ratingForIndex(_ i : Int) -> Rating? {
        switch i {
        case 0:
            return nil
        case 1:
            return .one
        case 2:
            return .two
        case 3:
            return .three
        case 4:
            return .four
        case 5:
            return .five
        default:
            return nil
        }
    }
    
    static func sortCriteria(_ i : Int) -> SortCriteria? {
        switch i {
        case 1:
            return .byDate
        case 2:
            return .rating
        default:
            return nil
        }
    }
    
    static func sortDirection(_ i : Int) -> SortDirection? {
        switch i {
        case 1:
            return .ascending
        case 2:
            return .descending
            
        default:
            return nil
        }
    }
}

extension ReviewQuery {
    func title() -> String {
        var t = Rating.noStars.description.capitalized
        if let r = rating {
            t = r.description.capitalized
        }
        
        if let sc = sortCriteria {
            t += " | Sorted " + sc.description.capitalized
        } else {
            t += " | Default sorting"
        }
        
        if let sd = sortDirection {
            t += " | " + sd.description.capitalized + " order"
        } else {
            t += " | Default order"
        }
        
        
        return t
    }
}
