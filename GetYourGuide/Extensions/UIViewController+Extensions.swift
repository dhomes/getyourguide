//
//  UIViewController+Extensions.swift
//  GetYourGuide
//
//  Created by dhomes on 7/13/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import UIKit
import SnapKit

extension UIViewController {
    func setTitleView() {
        let imageView = UIImageView(image: Styles.images.logo)
        imageView.tintColor = Styles.colors.tintColor
        imageView.snp.makeConstraints { (m) in
            m.width.equalTo(40)
            m.height.equalTo(40)
        }
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.title = nil
        self.navigationItem.titleView = imageView
    }
}
