//
//  Network.swift
//  GetYourGuide
//
//  Created by dhomes on 7/12/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation
import Just

typealias ReviewCompletion = (Result<[Review],NetworkError>) -> ()
typealias DataCompletion = (Result<Data,NetworkError>) -> ()

/// Handles network requests
struct Network {

    /// Executes a network request
    ///
    /// - Discussion: completion block is executed in passed OR calling queue (avoids repeating OperationQueue.main.asych)
    ///
    /// - Parameters:
    ///   - request: Request to execute
    ///   - completion: a .success(Data), .failure(Error) completion
    func getData(for request : Request,
                 returnQueue : OperationQueue? = nil,
                 completion : @escaping DataCompletion) {
        
        let q = returnQueue ?? OperationQueue.current ?? OperationQueue.main
        
        self.execute(request, requestBody: nil) { (result) in
            switch result {
            case .failure(let e):
                q.addOperation {
                    completion(.failure(e))
                }
                return
            case .success(let data):
                q.addOperation {
                    completion(.success(data))
                }
                return
            }
        }
        
    }

}

private extension Network {

    func execute(_ request : Request,
                 method : HTTPMethod = .post,
                 requestBody : [String : Any]? = nil,
                 additionalHeaders : [String : String]? = nil,
                 completion : DataCompletion?) {
        
        let url = request.endpoint()
        var headers = Network.defaultHeaders
        if let additional = additionalHeaders {
            additional.forEach {
                headers[$0.key] = $0.value
            }
        }
        let data = requestBody?.data()
        
        Just.request(method, url: url, headers: headers, requestBody: data) { (result) in
            result.log()
            if let e = result.error {
                completion?(.failure(NetworkError.other(e)))
            } else {
                guard let data = result.content else {
                    completion?(.failure(NetworkError.noData))
                    return
                }
                completion?(.success(data))
            }
        }
    }
}

private extension Network {
    
    static var defaultHeaders : [String : String] {
        return ["Content-Type" : "application/json",
                "Accept" : "application/json"]
    }
}
