//
//  QueryRepresentable.swift
//  GetYourGuide
//
//  Created by dhomes on 7/12/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation

protocol QueryRepresentable {
    var query : [String : Any] { get }
}

