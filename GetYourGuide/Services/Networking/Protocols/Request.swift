//
//  EndpointType.swift
//  GetYourGuide
//
//  Created by dhomes on 7/12/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation

protocol Request {
    func urlString() -> String
    func endpoint() -> String
    func base() -> String
    var rawValue : String { get }
    var path : String { get }
    var urlQuery : String { get }
}

extension Request {
    
    func urlString() -> String {
        return self.endpoint() + self.rawValue
    }
    
    func base() -> String {
        return "https://www.getyourguide.com/"
    }
    
    func endpoint() -> String {
        return base() + self.path + self.rawValue + self.urlQuery
    }
}
