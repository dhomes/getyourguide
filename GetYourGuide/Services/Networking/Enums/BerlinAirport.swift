//
//  Enums.swift
//  GetYourGuide
//
//  Created by dhomes on 7/12/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation

/// The requests we can go on Berlin Airport
///
/// - site: for fetching the known page for the location
/// - reviews: for fetching the reviews
enum BerlinAirport : Request {
    
    case site
    case reviews(ReviewQuery)
    
    var rawValue: String {
        switch self {
        case .site:
            return ""
        case .reviews:
            return "reviews.json"
        }
    }
    
    var path: String {
        switch self {
        case .site, .reviews:
            return "berlin-l17/tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776/"
        }
    }
    
    var urlParameters: [String : Any]? {
        switch self {
        case .reviews(let queryRepresentable):
            return queryRepresentable.query
        default:
            return nil
        }
    }
    
    var urlQuery: String {
        switch self {
        case .site:
            return ""
        case .reviews(let reviewQuery):
            return reviewQuery.query.urlQuery()
        }
        
    }
    
}

