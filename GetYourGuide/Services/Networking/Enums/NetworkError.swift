//
//  NetworkError.swift
//  GetYourGuide
//
//  Created by dhomes on 7/12/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation

/// NetworkError : Error
enum NetworkError : Error, CustomStringConvertible {
    case networkUnreachable
    case noData
    case other(Error)
    
    var description: String {
        switch self {
        case .networkUnreachable:
            return "Unreachable network"
        case .noData:
            return "No reponse"
        case .other(let e):
            if let gyge = e as? GYGError {
                return gyge.message
            }
            return e.localizedDescription
        }
    }
}

extension Error {
    var description : String {
        if let e = self as? NetworkError {
            return e.description
        } else {
            return self.localizedDescription
        }
    }
}

extension Error {
    var shouldPresent : Bool {
        if let e = self as? NetworkError {
            switch e {
            case .other(let containedError):
                if let gygerror = containedError as? GYGError {
                    // it's not clear if status == false for ANY backed error or only when there is no more reviews to fetch, so hardcoding the known string for this case
                    return gygerror.message != "Could not find data for the requested tour."
                }
            default:
                return true
            }
        }
        return true
    }
}
