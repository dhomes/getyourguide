//
//  Styles.swift
//  GetYourGuide
//
//  Created by dhomes on 7/13/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import UIKit
import Hex

struct Styles {
    
    struct colors {
        static let barTintColor = UIColor(hex: "FF5533")
        static let tintColor = UIColor.white
        static let lightGray = UIColor(hex: "f1f1f1")
        static let webGray1 = UIColor(hex: "f0f4f7")
        static let ultraLightGray = UIColor(hex: "fbfbfb")
        static let lightGray2 = UIColor(hex: "f8f8f8")
        static let mediumGray = UIColor(hex: "4e4d4e")
    }
    
    struct images {
        static let logo = UIImage(named: "logo")?.withRenderingMode(.alwaysTemplate)
        static let gear = UIImage(named: "gear")?.withRenderingMode(.alwaysTemplate)
    }
    
    struct fonts {
        static let regularMedium = UIFont.systemFont(ofSize: 14, weight: .regular)
    }
    
    static func theme() {
        UINavigationBar.appearance().barTintColor = colors.barTintColor
        UINavigationBar.appearance().tintColor = colors.tintColor
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : colors.tintColor]
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().shadowImage = UIImage()
    }
}
