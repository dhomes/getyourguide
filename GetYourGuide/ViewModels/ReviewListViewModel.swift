//
//  ReviewViewModel.swift
//  GetYourGuide
//
//  Created by dhomes on 7/13/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation
import Hydra
import ISMessages


/// The direction for the next fetch
///
/// - top: for pull to refresh
/// - bottom: bottom cell reached, query.page + 1 needed if there are more reviews available
enum FetchDirection {
    case top
    case bottom
}


/// Handles fetching, pagination & query state
class ReviewListViewModel {

    //MARK: - PRIVATE
    private var currentQuery : ReviewQuery? = nil
    private var query : ReviewQuery {
        guard let query = currentQuery else {
            let q = Defaults.getCustomObject(for: .reviewQuery) ?? ReviewQuery.defaultQuery
            currentQuery = ReviewQuery(count: q.count, initialPage: 0, rating: q.rating, sortCriteria: q.sortCriteria, sortDirection: q.sortDirection)
            return currentQuery!
        }
        return query
    }
    
    private var store = ReviewStore()
    private func updateReviews(with new : [ReviewViewModel], direction : FetchDirection) {
        guard new.count > 0 else {
            return
        }
        if direction == .bottom {
            let oldReviews = reviews.value
            let updatedReviews = oldReviews + new
            self.reviews.value = updatedReviews
        } else {
            self.reviews.value = new
        }
        self.refreshed.value = direction
        
    }

    //MARK: - INTERNAL
    init() {
        NotificationCenter.default.addObserver(forName: UIApplication.QUERY_SETTINGS_CHANGED, object: nil, queue: .main) { [weak self] (_) in
            self?.currentQuery = nil
            self?.fetch(direction: .top, completion: nil)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    var reviews = Observable<[ReviewViewModel]>([ReviewViewModel]())
    var refreshed = Observable<FetchDirection>(.top)
    
    var count : Int {
        return reviews.value.count
    }
    
    func rowsInSection(_ section : Int) -> Int {
        return count == 0 ? 0 : section == 0 ? 1 : count
    }
    
    func numberOfSections() -> Int {
        return count == 0 ? 0 : 2
    }
    
    func queryDescription() -> String {
        return self.query.title()
    }
    
    func reviewAt(indexPath : IndexPath) -> ReviewViewModel {
        return reviews.value[indexPath.row]
    }

    func fetch(direction : FetchDirection = .bottom, completion : (() -> ())? = nil) {

        if direction == .top {
            store = ReviewStore()
            query.page = 0
        }
        
        if !store.moreAvailable {
            completion?()
            return
        }

        store.getReviews(forQuery: query).then(in: .main) { [weak self] (reviews) in
            
            let reviewModels = reviews.compactMap { ReviewViewModel(withReview: $0) }
            self?.updateReviews(with: reviewModels, direction: direction)
            self?.query.increasePage()
            
            }.catch(in: .main) { (error) in
                if error.shouldPresent {
                    ISMessages.showCardAlert(withTitle: "Fetch error", message: error.description, duration: 3.0, hideOnSwipe: true, hideOnTap: true, alertType: .error, alertPosition: .top, didHide: nil)
                }

            }.always(in: .main) {
                completion?()
        }
        
    }
    
    func fetchIfNeeded(for row : Int, indicator : UIActivityIndicatorView?) {
        if !store.moreAvailable { return }
        if row == reviews.value.count - 1 {
            indicator?.alpha = 1
            indicator?.startAnimating()
            fetch(direction: .bottom)
        }
    }
  
    @objc func refresh(_ refresh : UIRefreshControl) {
        self.fetch(direction: .top) {}
    }
}
