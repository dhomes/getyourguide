//
//  HeaderViewModel.swift
//  GetYourGuide
//
//  Created by dhomes on 7/13/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import UIKit

/// Provides a header view after obtaining info for the default location (berlin airport) from either Defaults or remote site.
class HeaderViewModel {
    
    let headerView : Observable<HeaderView?> = Observable<HeaderView?>(nil)
    private let location : Observable<Location?> = Observable<Location?>(nil)
    
    init() {
        location.valueChanged = { [weak self] location in
            guard let l = location else {
                return
            }
            self?.headerView.value = HeaderView(withLocation: l)
        }
        
    }
    
    func getLocation() {
        LocationStore().info { (result) in
            switch result {
            case .success(let location):
                self.location.value = location
            default:
                return
            }
        }
    }
    
}
