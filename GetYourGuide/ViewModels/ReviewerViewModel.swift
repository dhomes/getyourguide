//
//  ReviewerViewModel.swift
//  GetYourGuide
//
//  Created by dhomes on 7/13/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation

/// Simple Reviewer struct
struct ReviewerViewModel {
    let author : String
    let reviewerName : String
    let reviewerCountry : String
    let reviewerFirstInital : String
    let reviewerPhotoLocation : String?
}
