//
//  ReviewViewModel.swift
//  GetYourGuide
//
//  Created by dhomes on 7/13/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import Foundation

/// Simple Model for cells, doesnt need bindings
struct ReviewViewModel {
    
    let rating : Double
    let title : String
    let date : Date
    let dateString : String
    let id : Int
    let language : String
    let message : String
    let reviewer : ReviewerViewModel
    
    init?(withReview review : Review) {
        
        // id is related to date created as well, higher id == newer date
        self.id = review.id

        self.rating = review.ratingDouble
        
        // We can show untitled reviews, no need to fail the initializer
        self.title = review.title ?? "Untitled"
        
        // we do want a date
        guard let date = review.date else {
            return nil
        }
        self.date = date
        self.dateString = review.dateString
        
        // may want to filter by this later
        self.language = review.languageCode
    
        self.message = review.message
        
        self.reviewer = ReviewerViewModel(author: review.author,
                                          reviewerName: review.reviewerName,
                                          reviewerCountry: review.reviewerCountry,
                                          reviewerFirstInital: review.firstInitial,
                                          reviewerPhotoLocation: review.reviewerProfilePhoto)
    }

}

extension ReviewViewModel {
    var reviewerText : String {
        return "\(reviewer.reviewerName) - \(reviewer.reviewerCountry) 𐄁 \(dateString)"
    }
}
extension ReviewViewModel : Equatable, Comparable {
    
    static func < (lhs: ReviewViewModel, rhs: ReviewViewModel) -> Bool {
        return lhs.id < rhs.id
    }
    
    static func == (lhs: ReviewViewModel, rhs: ReviewViewModel) -> Bool {
        return lhs.id == rhs.id
    }
}
