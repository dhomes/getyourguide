//
//  NetworkTests.swift
//  GetYourGuideTests
//
//  Created by dhomes on 7/12/19.
//  Copyright © 2019 Get Your Guide. All rights reserved.
//

import XCTest
import Just
@testable import GetYourGuide

class Tests: XCTestCase {

    
    override func setUp() {
    }

    override func tearDown() {
    }

    func testReviewDecoding() {
        let count = 100
        let testQuery = "https://www.getyourguide.com/berlin-l17/tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776/reviews.json?count=\(count)&page=0&rating=0&sortBy=date_of_review&direction=DESC"
    
        let e = XCTestExpectation(description: "GET REVIEWS")
        Just.get(testQuery) { (r) in
            guard let data = r.content else {
                XCTFail("no data available")
                return
            }
            let decoder = JSONDecoder()
            do {
                let container = try decoder.decode(ReviewsContainer.self, from: data)
                let reviews = container.reviews
                print(reviews.count)
                XCTAssert(reviews.count == count)
            } catch {
                print(error.localizedDescription)
            }
            e.fulfill()
        }
        wait(for: [e], timeout: 10)
    
    }

    func testGetSite() {
        let request = BerlinAirport.site
        let e = XCTestExpectation(description: "SITE EXPECTATION")
        Network().getData(for: request) { (result) in
            
            switch result {
            case .failure(let e):
                print(e.description)
                XCTFail("Error")
            case .success(let data):
                    guard let string = String(data: data, encoding: .utf8) else {
                        XCTFail("Could not conver")
                        return
                    }
                    print(string)
            }
            
            e.fulfill()
        }
        wait(for: [e], timeout: 10)
    }
    
    func testReviewQuery() {
        
       var query = ReviewQuery(count: 20, initialPage: 0, rating: nil, sortCriteria: nil, sortDirection: nil)
        
       let fetch1 = getReviews(forQuery: query)
       XCTAssert(fetch1.success)
       XCTAssert(fetch1.reviews.count == query.count)
       query.increasePage()
       
       let fetch2 = getReviews(forQuery: query)
       XCTAssert(fetch2.success)
       XCTAssert(fetch2.reviews.count == query.count)

        // check we have different set of reviews
        let ids1 = Set(fetch1.reviews.map { $0.id })
        let ids2 = Set(fetch2.reviews.map { $0.id })
        
        XCTAssert(ids1.intersection(ids2).count == 0)
        
        // try another query with above 3 rating, asc direction, by date sort review
        query = ReviewQuery(count: 20, initialPage: 0, rating: .four, sortCriteria: .byDate, sortDirection: .ascending)
        let fetch3 = getReviews(forQuery: query)
        XCTAssert(fetch3.success)
        XCTAssert(fetch3.reviews.count == query.count)
        let reviews = fetch3.reviews
        for (i,review) in reviews.enumerated() {
           
            if i > 0 {
                let r1 = reviews[i - 1]
                let r2 = review
                XCTAssert(r1.date != nil)
                XCTAssert(r2.date != nil)
                 print(r2.dateString)
                print(r2.date!.description(with: Locale.current))
                XCTAssert(r1.date! <= r2.date!)
            }
        }
        
    }
    
    func getReviews(forQuery query : ReviewQuery) -> (success: Bool, reviews : [Review]) {
        var success = false
        var reviews = [Review]()
        
        let e1 = XCTestExpectation(description: "1st fetch")
        let request = BerlinAirport.reviews(query)
        
        Network().getData(for: request) { (result) in
            switch result {
                
            case .failure(let e):
                print(e.description)
                XCTFail("Error")
                
            case .success(let data):
                do {
                    let decoder = JSONDecoder()
                    let container = try decoder.decode(ReviewsContainer.self, from: data)
                    let r = container.reviews
                    XCTAssert(r.count == query.count)
                    success = true
                    reviews = r
                } catch {
                    print(error.localizedDescription)
                }
                
            }
            
            e1.fulfill()
        }
        
        wait(for: [e1], timeout: 10)
        return (success, reviews)
    }

    func testDateParsing() {
        let dateString = "March 11, 2013"
        guard let date = Date.fromNamedMonthDayYear(dateString) else {
            XCTFail()
            return
        }
        let month = Calendar.current.component(.month, from: date)
        let day = Calendar.current.component(.day, from: date)
        let year = Calendar.current.component(.year, from: date)
        XCTAssert(month == 3)
        XCTAssert(day == 11)
        XCTAssert(year == 2013)
    }
    
    func testSiteParser() {
        
        let e = XCTestExpectation(description: "ok")
        e.expectedFulfillmentCount = 1
        
        LocationStore().info { (result) in
            
            switch result {
            case .failure(let e):
                print(e)
                XCTFail(e.description)
            case .success(let site):
                XCTAssert(site.image != nil)
                print(site)
                site.getImage(completion: { (image) in
                    XCTAssertNotNil(image)
                })
            }
            
            e.fulfill()
        }
        
        wait(for: [e], timeout: 1000)
    }
    
    func testStoreQuery() {
        Defaults.removeObject(for: .reviewQuery)
        
        let query = ReviewQuery(count: 15,
                                initialPage: 0,
                                rating: .four,
                                sortCriteria: .byDate,
                                sortDirection: .ascending)
        
        XCTAssert(Defaults.setCustomObject(query, forKey: .reviewQuery))
        
        let retrieved : ReviewQuery? = Defaults.getCustomObject(for: .reviewQuery)
        XCTAssertNotNil(retrieved)
        print(retrieved!)
        XCTAssert(retrieved!.count ==  15)
        XCTAssert(retrieved!.rating == .four)
        XCTAssert(retrieved!.sortCriteria == .byDate)
        XCTAssert(retrieved!.sortDirection == .ascending)
    }
}
